package br.com.galgo;

import java.util.List;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import br.com.galgo.enumerador.Execucao;
import br.com.galgo.interfaces.TestePos;
import br.com.galgo.interfaces.TestePosicaoAtivos;
import br.com.galgo.interfaces.TestePre;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.entidades.HistoricoEventos;

@Category(TestePosicaoAtivos.class)
public class ValidaHistoricoEventos {

	private final String PATH_HISTORICO = "K:/Condominio_STI/001 - Sist Galgo/Documentacao/10 Producao/Problemas/Eventos Pendentes/Validacao/Historico/Validacao.csv";

	@Category(TestePre.class)
	@Test
	public void verificarHistoricoPosicaoAtivosPre() throws ErroAplicacao {
		Servico servico = ArquivoUtils.getServicoHistorico(PATH_HISTORICO);
		VerificarHistoricoEventos verificarHistoricoEventos = new VerificarHistoricoEventos();
		List<HistoricoEventos> listaHistorico = verificarHistoricoEventos
				.verificarHistorico(Execucao.PRE, PATH_HISTORICO, servico);
		ArquivoUtils.gravaHistorico(PATH_HISTORICO, listaHistorico, servico);
	}

	@Category(TestePos.class)
	@Test
	public void verificarHistoricoPosicaoAtivosPos() throws ErroAplicacao {
		Servico servico = ArquivoUtils.getServicoHistorico(PATH_HISTORICO);
		VerificarHistoricoEventos verificarHistoricoEventos = new VerificarHistoricoEventos();
		List<HistoricoEventos> listaHistorico = verificarHistoricoEventos
				.verificarHistorico(Execucao.POS, PATH_HISTORICO, servico);
		ArquivoUtils.gravaHistorico(PATH_HISTORICO, listaHistorico, servico);
	}

}
