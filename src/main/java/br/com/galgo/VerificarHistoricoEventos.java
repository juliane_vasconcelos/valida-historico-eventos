package br.com.galgo;

import java.util.List;

import br.com.galgo.enumerador.Execucao;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.entidades.HistoricoEventos;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.compromisso.TelaPainelCompromissos;

public class VerificarHistoricoEventos {

	public List<HistoricoEventos> verificarHistorico(Execucao execucao,
			String caminho, Servico servico) throws ErroAplicacao {
		List<HistoricoEventos> listaHistorico = ArquivoUtils
				.uploadListaDatasParaValidacao(caminho);

		TelaGalgo.abrirBrowser(Ambiente.PRODUCAO.getUrl());
		TelaLogin telaLogin = new TelaLogin();

		final UsuarioConfig usuarioConfig = UsuarioConfig.MASTER_STI;
		Usuario usuario = new Usuario(usuarioConfig);
		TelaHome telaHome = telaLogin.loginAs(usuario);

		TelaPainelCompromissos telaPainelCompromissos = (TelaPainelCompromissos) telaHome
				.acessarSubMenu(SubMenu.PAINEL_COMPROMISSO);

		telaPainelCompromissos.esperarSegundos(6);
		telaPainelCompromissos.clicarFiltrosDeBusca();

		for (HistoricoEventos historicoEventos : listaHistorico) {
			final String qtdDeEventosPendentesConsolidado = verificarVisaoConsolidada(
					historicoEventos, telaPainelCompromissos, servico);

			final String qtdDeEventosPendentesNaoConsolidado = verificarVisaoNaoConsolidada(
					historicoEventos, telaPainelCompromissos, servico);

			if (Execucao.PRE == execucao) {
				populaEventosPre(historicoEventos,
						qtdDeEventosPendentesConsolidado,
						qtdDeEventosPendentesNaoConsolidado);
			} else {
				popularEventosPos(historicoEventos,
						qtdDeEventosPendentesConsolidado,
						qtdDeEventosPendentesNaoConsolidado);
			}
		}
		TelaGalgo.fecharBrowser();
		return listaHistorico;
	}

	private void populaEventosPre(HistoricoEventos historicoEventos,
			final String qtdDeEventosPendentesConsolidado,
			final String qtdDeEventosPendentesNaoConsolidado) {
		historicoEventos
				.setQtdEventosConsolidadoPre(qtdDeEventosPendentesConsolidado);
		historicoEventos
				.setQtdEventosNaoConsolidadaPre(qtdDeEventosPendentesNaoConsolidado);
	}

	private void popularEventosPos(HistoricoEventos historicoEventos,
			final String qtdDeEventosPendentesConsolidado,
			final String qtdDeEventosPendentesNaoConsolidado) {
		historicoEventos
				.setQtdEventosConsolidadoPos(qtdDeEventosPendentesConsolidado);
		historicoEventos
				.setQtdEventosNaoConsolidadoPos(qtdDeEventosPendentesNaoConsolidado);
	}

	private String verificarVisaoNaoConsolidada(
			HistoricoEventos historicoEventos,
			TelaPainelCompromissos telaPainelCompromissos, Servico servico) {
		preencheFiltrosBasicos(historicoEventos, telaPainelCompromissos,
				servico);
		telaPainelCompromissos.clicarVisaoNaoConsolidada();
		telaPainelCompromissos.clicarBotaoConfirmar();

		final int qtdDeEventosPendentes = telaPainelCompromissos
				.pegarQtdDeEventosPendentes();
		return String.valueOf(qtdDeEventosPendentes);
	}

	private String verificarVisaoConsolidada(HistoricoEventos historicoEventos,
			TelaPainelCompromissos telaPainelCompromissos, Servico servico) {
		preencheFiltrosBasicos(historicoEventos, telaPainelCompromissos,
				servico);
		telaPainelCompromissos.clicarVisaoConsolidada();
		telaPainelCompromissos.clicarBotaoConfirmar();

		final int qtdDeEventosPendentes = telaPainelCompromissos
				.pegarQtdDeEventosPendentes();
		return String.valueOf(qtdDeEventosPendentes);
	}

	private void preencheFiltrosBasicos(HistoricoEventos historicoEventos,
			TelaPainelCompromissos telaPainelCompromissos, Servico servico) {
		telaPainelCompromissos.preecherServico(servico);
		telaPainelCompromissos.preecherDataInicial(historicoEventos
				.getDataHistorico());
		telaPainelCompromissos.preecherDataFinal(historicoEventos
				.getDataHistorico());
	}

}
